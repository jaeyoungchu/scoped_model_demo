import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:scoped_model_demo/models/app_model.dart';
import 'package:scoped_model_demo/models/item_model.dart';

import 'display_page.dart';

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController controller = TextEditingController();

  String s='';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppModel a = ScopedModel.of<AppModel>(context);
    a.items.add(Item('z'));
    Future.delayed(const Duration(seconds: 1), (){
      ScopedModel.of<AppModel>(context).items.add(Item('zz'));
      print('delay end');
      ScopedModel.of<AppModel>(context).setLoading(true);
//      setState(() {
//        s='aa';
//      });
    }

    );

  }

  @override
  Widget build(BuildContext context) {
    print('build' );
//    Future.delayed(const Duration(seconds: 1), (){
//      ScopedModel.of<AppModel>(context).items.add(Item('aaaa'));
//      print('delay end');}
//    );
//    ScopedModel.of<AppModel>(context, rebuildOnChange: true).items.add(new Item('cc'));
//    delay(context);


    return Scaffold(
      appBar: AppBar(
        title: Text("Home Page"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              child: ScopedModelDescendant<AppModel>(
                builder: (context,child,model) => Column(
                  children: model.items.map(
                        (item)=> ListTile(
                      title: Text(item.name),
                      onLongPress: (){
                        model.deleteItem(item);
                      },
                    ),
                  ).toList(),
                ),
              ),
            ),
            TextField(
              controller: controller,
            ),
            ScopedModelDescendant<AppModel>(
              builder: (context,child,model){
                print('${model.items.length}');
                return RaisedButton(
                  child: Text("Add Item"),
                  onPressed: (){
                    Item item = Item(controller.text);
                    model.addItem(item);
//                    setState(() {
//                      controller.text = '';
//                    });
                  },
                );
              }
            ),
            RaisedButton(
              child: Text('Display Page'),
              onPressed: (){
                Navigator.push(context, new MaterialPageRoute(
                    builder: (context) =>
                    new DisplayPage())
                );
//                Navigator.of(context).push(MaterialPageRoute(builder: (context)=>DisplayPage()));
              },
            )
          ],

        ),
      ),
    );
  }

  delay(BuildContext context){
    print('delay start');
    Future.delayed(const Duration(seconds: 1), (){
      ScopedModel.of<AppModel>(context).items.add(Item('aaaa'));
      print('delay end');}
    );

  }
}
