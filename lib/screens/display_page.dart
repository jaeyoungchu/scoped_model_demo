import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:scoped_model_demo/models/app_model.dart';
import 'package:scoped_model_demo/models/item_model.dart';
class DisplayPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Display Page"),
      ),
      body: Container(
        child: ScopedModelDescendant<AppModel>(
            builder: (context,child,model) => Column(
              children: model.items.map(
                      (item)=> ListTile(
                        title: Text(item.name),
                        onLongPress: (){
                          model.deleteItem(item);
                        },
                      ),
              ).toList(),
            ),
        ),
      ),
    );
  }

  DisplayPage(){
    print("constructor");
//    ScopedModel.of<AppModel>(context, rebuildOnChange: true).items.add(new Item('aaa'));
//    Widget a = ScopedModelDescendant<AppModel>(
//      builder: (context,child,model){
//        print('builder');
//        model.addItem(new Item("con"));
//        return Text("asd");
//      },
//    );
  }
}
