import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import 'item_model.dart';

class AppModel extends Model{

  static AppModel of(BuildContext context) =>
      ScopedModel.of<AppModel>(context);

  List<Item> _items = [];
  List<Item> get items => _items;
  bool isLoading = false;
  void addItem(Item item) {
    print('add item ${item.name}');
    _items.add(item);
    notifyListeners();
  }

  setLoading(bool flag){
    isLoading = flag;
    notifyListeners();
  }

  void deleteItem(Item item){
    _items.remove(item);
    notifyListeners();
  }
}

