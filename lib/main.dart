import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:scoped_model_demo/screens/display_page.dart';
import 'package:scoped_model_demo/screens/home_page.dart';

import 'models/app_model.dart';
import 'models/item_model.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

//  final routes = <String,WidgetBuilder>{
//    "home-page" : (BuildContext context) => MyHomePage(),
//    "display-page" : (BuildContext context) => DisplayPage(),
//  };


  @override
  Widget build(BuildContext context) {
//    ScopedModel.of<AppModel>(context).items.add(Item('11'));
    return ScopedModel<AppModel>(
      model: AppModel(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        routes: <String,WidgetBuilder>{
          "home-page": (context) => MyHomePage(),
          "display-page": (context) => DisplayPage(), //you should have something like this.
        },
        home: MyHomePage(),
//      home: DefaultTabController(
//          length: 2,
//          child: ScopedModel<AppModel>(
//            model: AppModel(),
//            child: Scaffold(
//              appBar: AppBar(
//                title: Text("Scoped Model Demo"),
//                bottom: TabBar(
//                    tabs: <Widget>[
//                      Tab(
//                        icon: Icon(Icons.home),
//                        text: 'Home Page',
//                      ),
//                      Tab(
//                        icon: Icon(Icons.screen_rotation),
//                        text: 'Display',
//                      ),
//                    ],
//                ),
//              ),
//              body: TabBarView(
//                  children: <Widget>[
//                    MyHomePage(),
//                    DisplayPage(),
//                  ]
//              ),
//            ),
//          ),
//      ),
      ),
    );
  }
}

